package kr.re.keti.expandablecalendarexample;


/**
 * Created by eajang on 2015. 12. 1..
 */
public class TemperatureData {
    double mMin;
    double mMax;
    double mAverage;

    TemperatureData(double mMin, double mMax, double mAverage) {
        this.mMin = mMin;
        this.mMax = mMax;
        this.mAverage = mAverage;
    }
}
