package kr.re.keti.expandablecalendarexample;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by eajang on 2015. 12. 1..
 */
public class CustomExpandableAdapter extends BaseExpandableListAdapter{

    private Context mContext;
    private HashMap<Integer, String[]> mWeekList;
    private HashMap<String, TemperatureData> mTemperatureDataList;

    private TextView mBtnSun, mBtnMon, mBtnTues, mBtnWed, mBtnThur, mBtnFri, mBtnSat;

    private LayoutInflater mInflater;

    public CustomExpandableAdapter(Context context, HashMap<Integer, String[]> weekList, HashMap<String, TemperatureData> temperatureList) {
        this.mContext = context;
        this.mWeekList = weekList;
        this.mTemperatureDataList = temperatureList;
        mInflater = LayoutInflater.from(context);
    }

    public void updateData(HashMap<Integer, String[]> weekList, HashMap<String, TemperatureData> temperatureList) {
        this.mWeekList = weekList;
        this.mTemperatureDataList = temperatureList;
        notifyDataSetChanged();
    }

    @Override
    public int getGroupCount() {
        return mWeekList.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return 1;
    }

    @Override
    public Object getGroup(int groupPosition) {
        return mWeekList.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        String key = (groupPosition+1)+"";
        return mTemperatureDataList.get(key);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {


       if (convertView == null) {
           convertView = mInflater.inflate(R.layout.grid_layout_week, null);
       }

        mBtnSun = (TextView) convertView.findViewById(R.id.tv_01);
        mBtnMon = (TextView) convertView.findViewById(R.id.tv_02);
        mBtnTues = (TextView) convertView.findViewById(R.id.tv_03);
        mBtnWed = (TextView) convertView.findViewById(R.id.tv_04);
        mBtnThur = (TextView) convertView.findViewById(R.id.tv_05);
        mBtnFri = (TextView) convertView.findViewById(R.id.tv_06);
        mBtnSat = (TextView) convertView.findViewById(R.id.tv_07);

        String[] weekDay = mWeekList.get(groupPosition+1);
        mBtnSun.setText(weekDay[0]);
        mBtnMon.setText(weekDay[1]);
        mBtnTues.setText(weekDay[2]);
        mBtnWed.setText(weekDay[3]);
        mBtnThur.setText(weekDay[4]);
        mBtnFri.setText(weekDay[5]);
        mBtnSat.setText(weekDay[6]);


        return convertView;
    }

    private View getParentGenericView() {
        LayoutInflater inflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.grid_layout_week, null);

        return view;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        View view;
        if (convertView == null) {
            view = getChildGenericView();
        } else {
            view = convertView;
        }

        ListView lv = (ListView) view.findViewById(R.id.child_lv);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(mContext, android.R.layout.simple_list_item_1);
        lv.setAdapter(adapter);

        TemperatureData data = (TemperatureData)getChild(groupPosition, childPosition);
        adapter.add("average: "+data.mAverage);


        return view;
    }

    private View getChildGenericView() {
        LayoutInflater inflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.child_chart_view, null);
        return view;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }
}
