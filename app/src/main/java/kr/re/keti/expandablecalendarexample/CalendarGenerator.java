package kr.re.keti.expandablecalendarexample;

import android.util.Log;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by eajang on 2015. 12. 1..
 */
public class CalendarGenerator {

    private static Calendar mCalendar = Calendar.getInstance();
    private Calendar mLastMonthCalendar;
    private Calendar mNextMonthCalendar;

    public static HashMap<Integer, String[]> getMonthDataList(int year, int month) {

        HashMap<Integer, String[]> mResultData = new HashMap<>();

        mCalendar.set(Calendar.YEAR, year);
        mCalendar.set(Calendar.MONTH, month);
        mCalendar.set(Calendar.DAY_OF_MONTH, 1);

        int lastDay = mCalendar.getActualMaximum(Calendar.DATE);
        String[] arrDays = new String[7];
        int startDay = mCalendar.get(Calendar.DAY_OF_WEEK);
        int day = 1;
        int dayIndex = startDay-1;

        for (int i = 0; i < dayIndex; i++) {
            arrDays[i] = "";
        }

        for (int i = 1; i < lastDay+1; i++)
        {
            arrDays[dayIndex] = i+"";
            mResultData.put(mCalendar.get(Calendar.WEEK_OF_MONTH), arrDays);
            if (dayIndex == 6) {
                dayIndex = 0;
                arrDays = new String[7];
            } else dayIndex++;
            mCalendar.add(Calendar.DATE, 1);        // 하루 증가
        }

        return mResultData;
    }
}
