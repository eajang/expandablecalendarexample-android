package kr.re.keti.expandablecalendarexample;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Set;

/**
 * A placeholder fragment containing a simple view.
 */
public class MainActivityFragment extends Fragment implements View.OnClickListener {

    /* UI components */
    private ExpandableListView mExpLvCalendar;
    private CustomExpandableAdapter mCalAdapter;
    private HashMap<Integer, String[]> mWeekDataList;
    private TextView mTvThisMonth;
    private Button mBtnLastMonth, mBtnNextMonth;
    HashMap<String, TemperatureData> mTemperatureDataList;

    int mYear, mMonth, mToday;

    public MainActivityFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main, container, false);

        Calendar calendar = Calendar.getInstance();
        mYear = calendar.get(Calendar.YEAR);
        mMonth = calendar.get(Calendar.MONTH);
        mToday = calendar.get(Calendar.DAY_OF_MONTH);

        mWeekDataList = CalendarGenerator.getMonthDataList(mYear, mMonth);
        // Sample Data
        mTemperatureDataList = new HashMap<>();
        mTemperatureDataList.put("1", new TemperatureData(20,25,22));
        mTemperatureDataList.put("2", new TemperatureData(21,22,22.5));
        mTemperatureDataList.put("3", new TemperatureData(20,25,22));
        mTemperatureDataList.put("4", new TemperatureData(20,25,22));
        mTemperatureDataList.put("5", new TemperatureData(21,29,23.5));
        mTemperatureDataList.put("6", new TemperatureData(21,29,23.5));
        mTemperatureDataList.put("7", new TemperatureData(22,28,26.5));


        mTvThisMonth = (TextView) view.findViewById(R.id.tv_month);
        mTvThisMonth.setText(mYear+"."+(mMonth+1)+"");
        mBtnLastMonth = (Button) view.findViewById(R.id.btn_prev);
        mBtnLastMonth.setOnClickListener(this);
        mBtnNextMonth = (Button) view.findViewById(R.id.btn_next);
        mBtnNextMonth.setOnClickListener(this);
        mExpLvCalendar = (ExpandableListView) view.findViewById(R.id.expand_lv);
        mCalAdapter = new CustomExpandableAdapter(getActivity(), mWeekDataList, mTemperatureDataList);
        mExpLvCalendar.setAdapter(mCalAdapter);

        mExpLvCalendar.setGroupIndicator(null);


        return view;
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch(id){
            case R.id.btn_prev:
                mMonth--;
                if (mMonth < 0) {
                    mYear--;
                    mMonth = 11;
                }
                break;
            case R.id.btn_next:
                mMonth++;
                if (mMonth > 11) {
                    mYear++;
                    mMonth = 0;
                }
                break;
        }

        mTvThisMonth.setText(mYear+"."+(mMonth+1)+"");
        mWeekDataList = CalendarGenerator.getMonthDataList(mYear, mMonth);

        // init expand state
        for(int i = 0; i<mExpLvCalendar.getCount(); i++) {
            if (mExpLvCalendar.isGroupExpanded(i))
                mExpLvCalendar.collapseGroup(i);
        }
        mCalAdapter.updateData(mWeekDataList, mTemperatureDataList);

    }
}
